#include "../include/image.h"
#include <stdint.h>
#include <stdio.h>

#ifndef ASSIGNMENT_3_IMAGE_ROTATION_MASTER_ROTATOR_H
#define ASSIGNMENT_3_IMAGE_ROTATION_MASTER_ROTATOR_H

void set_pixel(const struct image* image, const uint64_t x, const uint64_t y, const struct pixel new_pixel);
struct pixel get_pixel(const struct image* image, const uint64_t x, const uint64_t y);

struct image rotate(const struct image* image);

#endif
