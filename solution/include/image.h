#include <stdint.h>
#include <stdio.h>

#ifndef ASSIGNMENT_3_IMAGE_ROTATION_MASTER_IMAGE_H
#define ASSIGNMENT_3_IMAGE_ROTATION_MASTER_IMAGE_H

#pragma pack(push, 1)
struct pixel {
    uint8_t b, g, r;
};
#pragma pack(pop)

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image create_image(const uint64_t width, const uint64_t height);

void destroy_image(struct image* image);

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)

enum read_status  {
    READ_OK = 0,
    READ_ERROR
};

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

uint8_t padding_count(uint8_t x);

struct bmp_header get_header(const struct image* image);

enum read_status from_bmp(FILE* const fileIn, struct image* image );
enum write_status to_bmp( FILE* fileOut, struct image const* image );

#endif 
