#include "../include/image.h"
#include "malloc.h"

#define ZERO 0
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_BIT_COUNT 24
#define BF_TYPE_SIZE 19778

struct image create_image(const uint64_t width, const uint64_t height){
    struct image image = {0};

    image.width = width;
    image.height = height;

    image.data = malloc(width * height * sizeof(struct pixel));
    return image;
}

void destroy_image(struct image* image){
    free(image->data);
    //.klhfglkjgyh
    //мы и так убиваем дату, зачем её ещё к нулу приравнивать?
    // это к код ревью
    //ура! моё первое использование коментов в си
    //зачем я это всё пишу?
    // gjqle gtkmvtytq ,f[ye]
    //пойду пельменей бахну
    //и возможно напьюсь
    //но это не точно
    //...
    //..
    //.
    //лучшее время для посадки крыжовника - ранняя осень
}

uint8_t padding_count(uint8_t x){
    return (4 - (sizeof(struct pixel) * x)) % 4;
}

struct bmp_header get_header(const struct image* image){

    size_t size = (image->width + padding_count(image->width)) * image->height * sizeof(struct pixel);

    return (struct bmp_header){
            .bfType = BF_TYPE_SIZE,
            .bfileSize = size + sizeof(struct bmp_header),
            .bfReserved = ZERO,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BI_SIZE,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = BI_PLANES,
            .biBitCount = BI_BIT_COUNT,
            .biCompression = ZERO,
            .biSizeImage = size,
            .biXPelsPerMeter = ZERO,
            .biYPelsPerMeter = ZERO,
            .biClrUsed = ZERO,
            .biClrImportant = ZERO,
    };
}

enum read_status from_bmp(FILE* const fileIn, struct image* image ){
    if (fileIn == NULL || image == NULL){
        return READ_ERROR;
    }

    struct bmp_header header = {0};

    if (fread(&header, sizeof(struct bmp_header), 1, fileIn) != 1){
        return READ_ERROR;
    }

    *image = create_image(header.biWidth, header.biHeight);

    uint8_t padding = padding_count(header.biWidth);

    for (size_t i = 0; i < image->height; ++i){
        if (fread(image->data + (i * image->width), sizeof(struct pixel), image->width, fileIn) != image->width) {
            destroy_image(image);
            return READ_ERROR;
        }

        if (fseek(fileIn, (long) padding, SEEK_CUR) != 0) {
            destroy_image(image);
            return READ_ERROR;
        }
    }
    return READ_OK;
}

enum write_status to_bmp( FILE* fileOut, struct image const* image ){
    if (fileOut == NULL || image == NULL){
        return WRITE_ERROR;
    }

    struct bmp_header header = get_header(image);

    if (!fwrite(&header, sizeof(struct bmp_header), 1, fileOut)){
        return WRITE_ERROR;
    }

    uint8_t padding = padding_count(image->width);
    uint8_t zero_padding = 0;

    for (size_t i = 0; i < image->height; i++){
        if (fwrite(image->data + (i * image->width), sizeof(struct pixel), image->width, fileOut) != image->width){
            return WRITE_ERROR;
        }

        for (size_t j = 0; j < padding; j++){
            if (!fwrite(&zero_padding, sizeof(uint8_t), 1, fileOut)){
                return WRITE_ERROR;
            }
        }
    }

    return WRITE_OK;
}
