#include "../include/image.h"
#include "../include/rotator.h"
#include <stdio.h>

int main( int argc, char** argv ) {
    if (argc != 3){
        printf("Needs 2 arguments");
        return 1;
    }

    FILE* fileInput = fopen(argv[1], "rb");

    struct image image = {0};

    enum read_status status = from_bmp(fileInput, &image);
    if (status != READ_OK){
        fprintf(stderr, "Error ocured while reading");
        return 1;
    }

    if (fclose(fileInput)){
        fprintf(stderr, "Unable to close file");
        return 1;
    };

    struct image imageOutput = rotate(&image);
    destroy_image(&image);

    FILE* fileOutput = fopen(argv[2], "wb");

    enum write_status writeStatus = to_bmp(fileOutput, &imageOutput);
    if (writeStatus != WRITE_OK){
        fprintf(stderr, "Unable to write file");
        return 1;
    }

    if (fclose(fileOutput)){
        fprintf(stderr, "Unable to close file");
        return 1;
    };

    destroy_image(&imageOutput);
    printf("Я сделаль! Я молодець! =)");
    return 0;
}
