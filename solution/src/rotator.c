#include "../include/rotator.h"

void set_pixel(const struct image* image, const uint64_t x, const uint64_t y, const struct pixel new_pixel){
    uint64_t arg = y * image->width + x;
    image->data[arg] = new_pixel;
}

struct pixel get_pixel(const struct image* image, const uint64_t x, const uint64_t y){
    uint64_t arg = y * image->width + x;
    return image->data[arg];
}

struct image rotate(const struct image* image){

    struct image resultImage = create_image(image->height, image->width);

    for (size_t i = 0; i < image->height; i++){
        for (size_t j = 0; j < image->width; j++){
            set_pixel(&resultImage, (image->height - 1) - i, j, get_pixel(image, j, i));
        }
    }
    return resultImage;
}
